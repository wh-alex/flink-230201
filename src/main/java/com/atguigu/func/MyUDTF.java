package com.atguigu.func;

import org.apache.flink.table.annotation.DataTypeHint;
import org.apache.flink.table.annotation.FunctionHint;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;

//@FunctionHint(output = @DataTypeHint("ROW<word1 STRING, word2 STRING>"))
@FunctionHint(output = @DataTypeHint("ROW<word1 STRING>"))
public class MyUDTF extends TableFunction<Row> {

    //value:hello-atguigu_hello-flink
    //value:hello-atguigu-hello-flink
    public void eval(String value) {
//        String[] split = value.split("_");
//        for (String s : split) {
//            String[] words = s.split("-");
//            collect(Row.of(words[0], words[1]));
//        }

        String[] split = value.split("-");
        for (String word : split) {
            collect(Row.of(word));
        }
    }
}
