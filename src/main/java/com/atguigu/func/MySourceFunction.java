package com.atguigu.func;

import org.apache.flink.streaming.api.functions.source.SourceFunction;

public class MySourceFunction implements SourceFunction<String> {

    private Boolean start = true;

    @Override
    public void run(SourceContext<String> ctx) throws Exception {
        int i = 0;
        while (start) {
            ctx.collect("id:" + i++);
            Thread.sleep(1000L);
        }
    }

    @Override
    public void cancel() {
        start = false;
    }
}
