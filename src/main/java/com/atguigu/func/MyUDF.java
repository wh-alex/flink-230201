package com.atguigu.func;

import org.apache.flink.table.functions.ScalarFunction;

public class MyUDF extends ScalarFunction {

    public String eval(String value) {
        return value.toUpperCase();
    }

}
