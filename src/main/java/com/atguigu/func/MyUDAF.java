package com.atguigu.func;


import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.table.functions.AggregateFunction;

public class MyUDAF extends AggregateFunction<Double, Tuple2<Double, Integer>> {

    @Override
    public Tuple2<Double, Integer> createAccumulator() {
        return new Tuple2<>(0.0D, 0);
    }

    @Override
    public Double getValue(Tuple2<Double, Integer> accumulator) {
        return accumulator.f0 / accumulator.f1;
    }

    public void accumulate(Tuple2<Double, Integer> acc, Double value) {
        acc.f0 = acc.f0 + value;
        acc.f1 = acc.f1 + 1;
    }

    public void retract(Tuple2<Double, Integer> acc, Double value) {
        acc.f0 = acc.f0 - value;
        acc.f1 = acc.f1 - 1;
    }

    public void merge(Tuple2<Double, Integer> acc, Iterable<Tuple2<Double, Integer>> it) {
        for (Tuple2<Double, Integer> tuple2 : it) {
            acc.f0 = acc.f0 + tuple2.f0;
            acc.f1 = acc.f1 + tuple2.f1;
        }
    }

    public void resetAccumulator(Tuple2<Double, Integer> acc) {
        acc.f0 = 0.0D;
        acc.f1 = 0;
    }
}
