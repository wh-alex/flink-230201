package com.atguigu.day12;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Schema;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import java.time.Duration;

public class Flink04_StreamToTable_EventTime {

    public static void main(String[] args) {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //从端口读取数据创建流
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将流转换为JavaBean,同时提取时间戳生成WaterMark
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor()).assignTimestampsAndWatermarks(WatermarkStrategy.<WaterSensor>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<WaterSensor>() {
            @Override
            public long extractTimestamp(WaterSensor element, long recordTimestamp) {
                return element.getTs();
            }
        }));

        //将流转换为动态表 1 必须为JavaBean
        //tableEnv.createTemporaryView("ws1", waterSensorDS);
        //执行查询
        //tableEnv.sqlQuery("select * from ws1").execute().print();

        //将流转换为动态表 2 最好为JavaBean
//        Table table = tableEnv.fromDataStream(waterSensorDS, $("id"), $("ts"), $("vc"), $("rt").rowtime());
        Table table = tableEnv.fromDataStream(waterSensorDS, Schema.newBuilder()
                .column("id", DataTypes.STRING())
                .column("ts", DataTypes.BIGINT())
                .column("vc", DataTypes.DOUBLE())
                .columnByMetadata("rowtime", "TIMESTAMP_LTZ(3)")
                //.watermark("rowtime", "SOURCE_WATERMARK()")
                .build());
        tableEnv.createTemporaryView("ws2", table);
        //执行查询
        //tableEnv.sqlQuery("select id,ts,vc,rowtime from ws2").execute().print();

        tableEnv.sqlQuery("" +
                        "SELECT \n" +
                        "    window_start, \n" +
                        "    window_end,\n" +
                        "    id,\n" +
                        "    SUM(vc)\n" +
                        "FROM TABLE(\n" +
                        "    TUMBLE(TABLE ws2, DESCRIPTOR(rowtime), INTERVAL '10' second))\n" +
                        "GROUP BY window_start, window_end,id;")
                .execute()
                .print();

    }

}
