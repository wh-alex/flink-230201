package com.atguigu.day12;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import static org.apache.flink.table.api.Expressions.$;

public class Flink05_StreamToTable_ProcessingTime {

    public static void main(String[] args) {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //从端口读取数据创建流
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将流转换为JavaBean,同时提取时间戳生成WaterMark
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //将流转换为动态表 1 必须为JavaBean
        //tableEnv.createTemporaryView("ws1", waterSensorDS);
        //执行查询
        //tableEnv.sqlQuery("select * from ws1").execute().print();

        //将流转换为动态表 2 最好为JavaBean
        Table table = tableEnv.fromDataStream(waterSensorDS, $("id"), $("ts"), $("vc"), $("pt").proctime());

        //没有提供对应的元数据信息
//        Table table = tableEnv.fromDataStream(waterSensorDS, Schema.newBuilder()
//                .column("id", DataTypes.STRING())
//                .column("ts", DataTypes.BIGINT())
//                .column("vc", DataTypes.DOUBLE())
//                .columnByMetadata("pt", "TIMESTAMP_LTZ(3)")
//                .build());
        tableEnv.createTemporaryView("ws2", table);
        //执行查询
        tableEnv.sqlQuery("select id,ts,vc,pt from ws2").execute().print();

    }

}
