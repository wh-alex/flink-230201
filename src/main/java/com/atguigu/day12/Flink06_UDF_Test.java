package com.atguigu.day12;

import com.atguigu.func.MyUDF;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class Flink06_UDF_Test {

    public static void main(String[] args) {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //获取表执行环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //创建表
        tableEnv.executeSql("" +
                "CREATE TABLE kafka_table_pt (\n" +
                "  `id` STRING,\n" +
                "  `ts` BIGINT,\n" +
                "  `vc` DOUBLE,\n" +
                "  `pt` AS PROCTIME()\n" +
                ") WITH (\n" +
                "  'connector' = 'kafka',\n" +
                "  'topic' = 'test',\n" +
                "  'properties.bootstrap.servers' = 'hadoop102:9092',\n" +
                "  'properties.group.id' = 'test_230201',\n" +
                "  'scan.startup.mode' = 'latest-offset',\n" +
                "  'format' = 'csv'\n" +
                ")");

        //注册自定义函数
        tableEnv.createTemporarySystemFunction("my_udf", MyUDF.class);

        //使用函数
        tableEnv.sqlQuery("select id,my_udf(id),vc from kafka_table_pt")
                .execute()
                .print();

    }

}
