package com.atguigu.day07;

import com.alibaba.fastjson.JSONObject;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.ConnectedStreams;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.CoMapFunction;
import org.apache.flink.streaming.api.functions.co.CoProcessFunction;
import org.apache.flink.util.Collector;

public class Flink06_ConnectProcessFunction {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream0 = env.socketTextStream("hadoop102", 7777);
        SingleOutputStreamOperator<JSONObject> socketTextStream1 = env.socketTextStream("hadoop102", 8888).map(new MapFunction<String, JSONObject>() {
            @Override
            public JSONObject map(String value) throws Exception {
                return JSONObject.parseObject(value);
            }
        });

        //使用Connect连接两个流
        ConnectedStreams<String, JSONObject> connectDS = socketTextStream0.connect(socketTextStream1);
        SingleOutputStreamOperator<Tuple2<String, Double>> resultDS = connectDS.process(new CoProcessFunction<String, JSONObject, Tuple2<String, Double>>() {
            @Override
            public void processElement1(String value, CoProcessFunction<String, JSONObject, Tuple2<String, Double>>.Context ctx, Collector<Tuple2<String, Double>> out) throws Exception {
                String[] split = value.split(",");
                out.collect(Tuple2.of(split[0], Double.parseDouble(split[2])));
            }

            @Override
            public void processElement2(JSONObject value, CoProcessFunction<String, JSONObject, Tuple2<String, Double>>.Context ctx, Collector<Tuple2<String, Double>> out) throws Exception {
                out.collect(Tuple2.of(value.getString("id"), value.getDouble("vc")));
            }
        });

        //打印
        socketTextStream0.print("000>>>");
        socketTextStream1.print("111>>>");
        resultDS.print("resultDS>>>");

        //启动
        env.execute();
    }

}
