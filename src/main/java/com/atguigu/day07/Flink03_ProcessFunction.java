package com.atguigu.day07;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

public class Flink03_ProcessFunction {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //创建侧输出流标签
        OutputTag<String> outputTag = new OutputTag<String>("vc>30") {
        };
        OutputTag<Double> outputTag1 = new OutputTag<Double>("vc>20") {
        };

        SingleOutputStreamOperator<WaterSensor> processDS = socketTextStream.process(new ProcessFunction<String, WaterSensor>() {

            @Override
            public void open(Configuration parameters) throws Exception {
//                getRuntimeContext();
            }

            @Override
            public void processElement(String value, ProcessFunction<String, WaterSensor>.Context ctx, Collector<WaterSensor> out) throws Exception {
//                ctx.output("aa", value);
//                ctx.output("bb", value);
                TimerService timerService = ctx.timerService();

//                long currentProcessingTime = timerService.currentProcessingTime();
//                timerService.registerProcessingTimeTimer(currentProcessingTime + 5000L);

                String[] split = value.split(",");

                //判断,如果水位线超过30,则输出到侧输出流
                if (Double.parseDouble(split[2]) > 30.0D) {
                    ctx.output(outputTag, value);
                } else if (Double.parseDouble(split[2]) > 20.0D) {
                    ctx.output(outputTag1, Double.parseDouble(split[2]));
                } else {
                    out.collect(new WaterSensor(split[0], Long.parseLong(split[1]), Double.parseDouble(split[2])));
                }
            }

            @Override
            public void close() throws Exception {

            }
        });

        //获取侧输出流的数据
        processDS.getSideOutput(outputTag).print("OutPut>>>");
        processDS.getSideOutput(outputTag1).print("output1>>>");

        processDS.print("主流>>>>");

        env.execute();
    }

}
