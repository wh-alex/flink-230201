package com.atguigu.day07;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;
import java.util.*;

public class Flink07_WindowProcess_TopN {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口获取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //提取时间戳生成WaterMark
        SingleOutputStreamOperator<WaterSensor> waterSensorWithWMDS = waterSensorDS.assignTimestampsAndWatermarks(WatermarkStrategy.<WaterSensor>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<WaterSensor>() {
            @Override
            public long extractTimestamp(WaterSensor element, long recordTimestamp) {
                return element.getTs();
            }
        }));

        //开窗、聚合:计算水位线出现次数TopN
        SingleOutputStreamOperator<String> resultDS = waterSensorWithWMDS.windowAll(SlidingEventTimeWindows.of(Time.seconds(10), Time.seconds(5)))
                .process(new ProcessAllWindowFunction<WaterSensor, String, TimeWindow>() {
                    @Override
                    public void process(ProcessAllWindowFunction<WaterSensor, String, TimeWindow>.Context context, Iterable<WaterSensor> elements, Collector<String> out) throws Exception {

                        //创建HashMap用于存放每种VC出现的次数
                        HashMap<Double, Integer> vcCountMap = new HashMap<>();

                        //遍历数据集
                        Iterator<WaterSensor> iterator = elements.iterator();
                        while (iterator.hasNext()) {
                            WaterSensor waterSensor = iterator.next();
                            Double vc = waterSensor.getVc();
                            if (vcCountMap.containsKey(vc)) {
                                vcCountMap.put(vc, vcCountMap.get(vc) + 1);
                            } else {
                                vcCountMap.put(vc, 1);
                            }
                        }

                        //对数据排序
                        ArrayList<Tuple2<Double, Integer>> result = new ArrayList<>();
                        Set<Map.Entry<Double, Integer>> entries = vcCountMap.entrySet();
                        for (Map.Entry<Double, Integer> entry : entries) {
                            result.add(new Tuple2<>(entry.getKey(), entry.getValue()));
                        }
                        result.sort(new Comparator<Tuple2<Double, Integer>>() {
                            @Override
                            public int compare(Tuple2<Double, Integer> o1, Tuple2<Double, Integer> o2) {
                                //从大到小排序
                                return o2.f1 - o1.f1;
                            }
                        });

                        StringBuilder stringBuilder = new StringBuilder();
                        //输出
                        for (int i = 0; i < Math.min(2, result.size()); i++) {
                            Tuple2<Double, Integer> vcCount = result.get(i);
                            stringBuilder.append("Top" + (i + 1) + ":Vc:" + vcCount.f0 + ",Count:" + vcCount.f1);
                            stringBuilder.append("\n");
                        }

                        //输出结果
                        out.collect(stringBuilder.toString());
                    }
                });

        //打印输出
        resultDS.print(">>>>");

        //启动
        env.execute();
    }
}
