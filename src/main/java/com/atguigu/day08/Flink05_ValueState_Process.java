package com.atguigu.day08;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

public class Flink05_ValueState_Process {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //按照ID进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //使用状态编程计算:如果同一个传感器连续两条数据都超过10,则输出警告信息到侧输出流！
        OutputTag<String> outputTag = new OutputTag<String>("side") {
        };
        SingleOutputStreamOperator<WaterSensor> resultDS = keyedStream.process(new KeyedProcessFunction<String, WaterSensor, WaterSensor>() {

            //声明状态
            private ValueState<Double> valueState;

            @Override
            public void open(Configuration parameters) throws Exception {
                //初始化状态
                valueState = getRuntimeContext().getState(new ValueStateDescriptor<Double>("vc-state", Double.class));
            }

            @Override
            public void processElement(WaterSensor value, KeyedProcessFunction<String, WaterSensor, WaterSensor>.Context ctx, Collector<WaterSensor> out) throws Exception {

                //获取状态数据以及当前数据中的VC
                Double lastVc = valueState.value();
                Double curVc = value.getVc();

                valueState.update(curVc);
                if (lastVc != null && lastVc > 10.0D && curVc > 10.0D) {
                    ctx.output(outputTag, "报警信息:" + value.getId() + "连续两条水位超过10！");
                }

                //将所有数据正常输出到主流
                out.collect(value);
            }
        });

        //打印
        resultDS.print("主流>>>>>");
        resultDS.getSideOutput(outputTag).print("SideOut>>>>>");

        //启动任务
        env.execute();

    }

}
