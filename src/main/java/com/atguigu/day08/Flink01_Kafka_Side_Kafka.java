package com.atguigu.day08;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SideOutputDataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.Properties;

public class Flink01_Kafka_Side_Kafka {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从Kafka读取数据
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "hadoop102:9092");
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "kafka_side_kafka");
        DataStreamSource<String> kafkaDS = env.addSource(new FlinkKafkaConsumer<String>("topic1", new SimpleStringSchema(), properties));

        kafkaDS.print("kafkaDS>>>>>");

        //分流,侧输出流(process)
        OutputTag<String> outputTag = new OutputTag<String>("side") {
        };
        SingleOutputStreamOperator<String> processDS = kafkaDS.process(new ProcessFunction<String, String>() {
            @Override
            public void processElement(String value, ProcessFunction<String, String>.Context ctx, Collector<String> out) throws Exception {
                //取出VC
                double vc = Double.parseDouble(value.split(",")[2]);
                if (vc < 30.0D) {
                    out.collect(value);
                } else {
                    ctx.output(outputTag, value);
                }
            }
        });

        //提取侧输出流数据
        SideOutputDataStream<String> sideOutput = processDS.getSideOutput(outputTag);

        //将数据写出(主流 -> topic2) (侧输出流 -> topic3)
        KafkaSink<String> topic2 = KafkaSink.<String>builder()
                .setBootstrapServers("hadoop102:9092")
                .setRecordSerializer(KafkaRecordSerializationSchema.<String>builder()
                        .setTopic("topic2")
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build())
                .build();
        processDS.sinkTo(topic2);

        sideOutput.addSink(new FlinkKafkaProducer<String>("hadoop102:9092", "topic3", new SimpleStringSchema()));

        //启动任务
        env.execute();

    }

}
