package com.atguigu.day09;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink06_ListState_Op {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(2);

        //开启CheckPoint
        //env.enableCheckpointing(5000L);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //在map算子中计算数据的个数
        SingleOutputStreamOperator<Integer> resultDS = waterSensorDS.map(new MyMapFunction());

        //打印
        resultDS.print();

        //启动
        env.execute();
    }

    public static class MyMapFunction implements MapFunction<WaterSensor, Integer>, CheckpointedFunction {

        private ListState<Integer> listState;
        private Integer count = 0;

        @Override
        public Integer map(WaterSensor value) throws Exception {
            count++;
            return count;
        }

        @Override
        public void snapshotState(FunctionSnapshotContext context) throws Exception {
            System.out.println("snapshotState>>>>>>>");
            listState.clear();
            listState.add(count);
        }

        @Override
        public void initializeState(FunctionInitializationContext context) throws Exception {
            System.out.println("initializeState>>>>>>");
            listState = context.getOperatorStateStore().getListState(new ListStateDescriptor<Integer>("list-state", Integer.class));
            Iterable<Integer> iterable = listState.get();
            for (Integer value : iterable) {
                count += value;
            }
        }
    }

}
