package com.atguigu.day09;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.AggregatingState;
import org.apache.flink.api.common.state.AggregatingStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink04_AggregatingState {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //按照ID进行分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //计算每种传感器的平均水位
        SingleOutputStreamOperator<Tuple2<String, Double>> resultDS = keyedStream.map(new RichMapFunction<WaterSensor, Tuple2<String, Double>>() {

            private AggregatingState<Double, Double> aggregatingState;

            @Override
            public void open(Configuration parameters) throws Exception {
                aggregatingState = getRuntimeContext().getAggregatingState(new AggregatingStateDescriptor<Double, Tuple2<Double, Integer>, Double>("agg-state", new AggregateFunction<Double, Tuple2<Double, Integer>, Double>() {
                    @Override
                    public Tuple2<Double, Integer> createAccumulator() {
                        return new Tuple2<>(0.0D, 0);
                    }

                    @Override
                    public Tuple2<Double, Integer> add(Double value, Tuple2<Double, Integer> accumulator) {
                        accumulator.f0 = accumulator.f0 + value;
                        accumulator.f1 = accumulator.f1 + 1;
                        return accumulator;
                    }

                    @Override
                    public Double getResult(Tuple2<Double, Integer> accumulator) {
                        return accumulator.f0 / accumulator.f1;
                    }

                    @Override
                    public Tuple2<Double, Integer> merge(Tuple2<Double, Integer> a, Tuple2<Double, Integer> b) {
                        a.f0 = a.f0 + b.f0;
                        a.f1 = a.f1 + b.f1;
                        return a;
                    }
                }, Types.TUPLE(Types.DOUBLE, Types.INT)));
            }

            @Override
            public Tuple2<String, Double> map(WaterSensor value) throws Exception {

                //将数据中的VC存入状态计算
                aggregatingState.add(value.getVc());

                //取出状态中的数据
                Double vc = aggregatingState.get();

                return new Tuple2<>(value.getId(), vc);
            }
        });

        //打印
        resultDS.print();

        //启动
        env.execute();
    }

}
