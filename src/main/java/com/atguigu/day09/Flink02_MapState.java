package com.atguigu.day09;

import com.atguigu.bean.WaterSensor;
import com.atguigu.func.StringToWaterSensor;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Iterator;
import java.util.Map;

public class Flink02_MapState {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据转换为JavaBean对象
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = socketTextStream.map(new StringToWaterSensor());

        //按照ID分组
        KeyedStream<WaterSensor, String> keyedStream = waterSensorDS.keyBy(WaterSensor::getId);

        //统计每种传感器每种水位值出现的次数
        SingleOutputStreamOperator<String> resultDS = keyedStream.map(new RichMapFunction<WaterSensor, String>() {

            private MapState<Double, Integer> mapState;

            @Override
            public void open(Configuration parameters) throws Exception {
                mapState = getRuntimeContext().getMapState(new MapStateDescriptor<Double, Integer>("map-state", Double.class, Integer.class));
            }

            @Override
            public String map(WaterSensor value) throws Exception {

                //取出当前数据中的VC
                Double vc = value.getVc();

                //判断当前VC是否存在于状态中
                if (mapState.contains(vc)) {
                    mapState.put(vc, mapState.get(vc) + 1);
                } else {
                    mapState.put(vc, 1);
                }

                //取出状态中所有数据
                Iterable<Map.Entry<Double, Integer>> entries = mapState.entries();
                Iterator<Map.Entry<Double, Integer>> iterator = entries.iterator();

                StringBuilder stringBuilder = new StringBuilder(value.getId())
                        .append(":").append("\n");

                while (iterator.hasNext()) {
                    Map.Entry<Double, Integer> next = iterator.next();
                    stringBuilder.append("    ")
                            .append(next.getKey())
                            .append(":")
                            .append(next.getValue())
                            .append("\n");
                }

                return stringBuilder.toString();
            }
        });

        //打印结果
        resultDS.print();

        //启动
        env.execute();

    }

}
