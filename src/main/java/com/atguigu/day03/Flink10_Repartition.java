package com.atguigu.day03;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Flink10_Repartition {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(2);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将并行度修改为2
        SingleOutputStreamOperator<String> mapDS = socketTextStream.map(new MapFunction<String, String>() {
            @Override
            public String map(String value) throws Exception {
                return value;
            }
        }).setParallelism(2);

        //打印
        mapDS.print("mapDS>>>>");

        //重分区
//        mapDS.shuffle().print("shuffle>>>>").setParallelism(4);
//        mapDS.rebalance().print("rebalance>>>").setParallelism(4);
//        mapDS.rescale().print("rescale>>>>").setParallelism(4);
//        mapDS.broadcast().print("broadcast>>>>").setParallelism(4);
//        mapDS.global().print("global>>>>").setParallelism(4);

        mapDS.forward().print("forward").setParallelism(2);

        //启动任务
        env.execute();
    }

}
