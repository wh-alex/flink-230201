package com.atguigu.day03;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Properties;

public class Flink03_Source_Kafka_Add {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //生产环境中,并行度设置为与主题的分区数一致
        env.setParallelism(1);

        //从Kafka test主题读取数据
        Properties properties = new Properties();
        //设置集群地址
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "hadoop102:9092");
        //设置消费者组
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "test_0201");
        DataStreamSource<String> streamSource = env.addSource(new FlinkKafkaConsumer<String>("test", new SimpleStringSchema(), properties));

        //打印数据
        streamSource.print();

        //启动
        env.execute();
    }
}
