package com.atguigu.day06;

import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.ProcessJoinFunction;
import org.apache.flink.streaming.api.windowing.assigners.ProcessingTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

public class Flink07_Join_IntervalJoin {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口获取数据
        //1001,1234,banzhang
        SingleOutputStreamOperator<Tuple3<String, Long, String>> ds1 = env.socketTextStream("hadoop102", 8888).map(new MapFunction<String, Tuple3<String, Long, String>>() {
            @Override
            public Tuple3<String, Long, String> map(String value) throws Exception {
                String[] split = value.split(",");
                return new Tuple3<>(split[0], Long.parseLong(split[1]), split[2]);
            }
        }).assignTimestampsAndWatermarks(WatermarkStrategy.<Tuple3<String, Long, String>>forMonotonousTimestamps().withTimestampAssigner(new SerializableTimestampAssigner<Tuple3<String, Long, String>>() {
            @Override
            public long extractTimestamp(Tuple3<String, Long, String> element, long recordTimestamp) {
                return element.f1;
            }
        }));
        //1001,1234,male
        SingleOutputStreamOperator<Tuple3<String, Long, String>> ds2 = env.socketTextStream("hadoop102", 9999).map(new MapFunction<String, Tuple3<String, Long, String>>() {
            @Override
            public Tuple3<String, Long, String> map(String value) throws Exception {
                String[] split = value.split(",");
                return new Tuple3<>(split[0], Long.parseLong(split[1]), split[2]);
            }
        }).assignTimestampsAndWatermarks(WatermarkStrategy.<Tuple3<String, Long, String>>forMonotonousTimestamps().withTimestampAssigner(new SerializableTimestampAssigner<Tuple3<String, Long, String>>() {
            @Override
            public long extractTimestamp(Tuple3<String, Long, String> element, long recordTimestamp) {
                return element.f1;
            }
        }));

        //intervalJoin
        SingleOutputStreamOperator<String> processDS = ds1.keyBy(new KeySelector<Tuple3<String, Long, String>, String>() {
                    @Override
                    public String getKey(Tuple3<String, Long, String> value) throws Exception {
                        return value.f0;
                    }
                }).intervalJoin(ds2.keyBy(new KeySelector<Tuple3<String, Long, String>, String>() {
                    @Override
                    public String getKey(Tuple3<String, Long, String> value) throws Exception {
                        return value.f0;
                    }
                })).between(Time.seconds(-5), Time.seconds(5))
                .process(new ProcessJoinFunction<Tuple3<String, Long, String>, Tuple3<String, Long, String>, String>() {
                    @Override
                    public void processElement(Tuple3<String, Long, String> left, Tuple3<String, Long, String> right, ProcessJoinFunction<Tuple3<String, Long, String>, Tuple3<String, Long, String>, String>.Context ctx, Collector<String> out) throws Exception {
                        out.collect(left.f0 + ",ts1:" + left.f1 + ",ts2:" + right.f1 + ",Name:" + left.f2 + ",Sex:" + right.f2);
                    }
                });

        //打印
        processDS.print();

        //启动
        env.execute();

    }

}
