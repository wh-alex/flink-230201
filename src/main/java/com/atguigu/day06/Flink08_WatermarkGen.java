package com.atguigu.day06;

import com.atguigu.bean.WaterSensor;
import com.atguigu.trigger.MyTrigger;
import org.apache.flink.api.common.eventtime.*;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

public class Flink08_WatermarkGen {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        System.out.println(env.getConfig().getAutoWatermarkInterval());
        env.getConfig().setAutoWatermarkInterval(1000L);

        //读取端口数据
//        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);
        KafkaSource<String> kafkaSource = KafkaSource.<String>builder()
                .setBootstrapServers("hadoop102:9092")
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .setTopics("test02")
                .setGroupId("flink08_watermarkgen")
                .setStartingOffsets(OffsetsInitializer.latest())
                .build();
        DataStreamSource<String> source = env.fromSource(kafkaSource, WatermarkStrategy.noWatermarks(), "kafka-source");

        //将数据转换为JavaBean
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = source.map(new MapFunction<String, WaterSensor>() {
            @Override
            public WaterSensor map(String value) throws Exception {
                String[] split = value.split(",");
                return new WaterSensor(split[0],
                        Long.parseLong(split[1]),
                        Double.parseDouble(split[2]));
            }
        });

        //自定义Watermark生成器
        SingleOutputStreamOperator<WaterSensor> waterSensorWithWMDS = waterSensorDS.assignTimestampsAndWatermarks(new WatermarkStrategy<WaterSensor>() {

            @Override
            public TimestampAssigner<WaterSensor> createTimestampAssigner(TimestampAssignerSupplier.Context context) {
                return new SerializableTimestampAssigner<WaterSensor>() {
                    @Override
                    public long extractTimestamp(WaterSensor element, long recordTimestamp) {
                        return element.getTs();
                    }
                };
            }

            @Override
            public WatermarkGenerator<WaterSensor> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
                return new WatermarkGenerator<WaterSensor>() {

                    private long delay = 5000L;
                    private long maxTs = Long.MIN_VALUE + delay;

                    @Override
                    public void onEvent(WaterSensor event, long eventTimestamp, WatermarkOutput output) {
                        //每条数据调用,提取所有数据中的最大时间戳
                        maxTs = Math.max(maxTs, event.getTs());
                        //output.emitWatermark(new Watermark(maxTs - delay));
                    }

                    @Override
                    public void onPeriodicEmit(WatermarkOutput output) {
                        output.emitWatermark(new Watermark(maxTs - delay));
                    }
                };
            }
        });

        //分组开窗、聚合
        SingleOutputStreamOperator<WaterSensor> sum = waterSensorWithWMDS.keyBy(WaterSensor::getId)
                .window(TumblingEventTimeWindows.of(Time.seconds(10)))
                .trigger(new MyTrigger(5000L))
                .max("vc");

        //打印
        sum.print();

        //启动
        env.execute();
    }

}
