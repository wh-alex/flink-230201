package com.atguigu.day13;

import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import com.ververica.cdc.connectors.mysql.table.StartupOptions;
import com.ververica.cdc.debezium.JsonDebeziumDeserializationSchema;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class FlinkCDC_Test {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        System.setProperty("HADOOP_USER_NAME", "atguigu");

        //开启CheckPoint
        env.enableCheckpointing(5000);
        env.getCheckpointConfig().setCheckpointStorage("hdfs://hadoop102:8020/flinkcdc");

        //创建MySQL-CDC Source
        MySqlSource<String> mySqlSource = MySqlSource.<String>builder()
                .hostname("hadoop103")
                .port(3306)
                .username("root")
                .password("000000")
                .databaseList("gmall-230201-flink")
                .tableList("gmall-230201-flink.base_trademark")  //要带上库名
                .deserializer(new JsonDebeziumDeserializationSchema())
                .startupOptions(StartupOptions.initial())
                .build();

        //使用Source读取数据
        DataStreamSource<String> dataStreamSource = env.fromSource(mySqlSource, WatermarkStrategy.noWatermarks(), "mysql-source");

        //打印
        dataStreamSource.print();

        //启动
        env.execute();

    }

}
