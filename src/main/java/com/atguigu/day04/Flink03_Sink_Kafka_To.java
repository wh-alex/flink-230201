package com.atguigu.day04;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import javax.annotation.Nullable;

public class Flink03_Sink_Kafka_To {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据写出到Kafka
//        KafkaSink<String> kafkaSink = KafkaSink.<String>builder()
//                .setBootstrapServers("hadoop102:9092")
//                .setRecordSerializer(KafkaRecordSerializationSchema.<String>builder()
//                        .setTopic("test")
//                        .setValueSerializationSchema(new SimpleStringSchema())
//                        .build())
//                .build();

        KafkaSink<String> kafkaSink = KafkaSink.<String>builder()
                .setBootstrapServers("hadoop102:9092")
                .setRecordSerializer(new KafkaRecordSerializationSchema<String>() {
                    @Nullable
                    @Override
                    public ProducerRecord<byte[], byte[]> serialize(String element, KafkaSinkContext context, Long timestamp) {
                        String[] split = element.split(" ");
                        return new ProducerRecord<>(split[0], split[1].getBytes());
                    }
                })
                .build();
        socketTextStream.sinkTo(kafkaSink);

//        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<String, String>();
//        kafkaProducer.send()

        //启动任务
        env.execute();
    }

}
