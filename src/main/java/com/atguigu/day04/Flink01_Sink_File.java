package com.atguigu.day04;

import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.configuration.MemorySize;
import org.apache.flink.connector.file.sink.FileSink;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.DateTimeBucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy;

import java.time.Duration;

public class Flink01_Sink_File {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //开启CheckPoint
        env.enableCheckpointing(5000L);

        //从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //将数据写出到文件
        FileSink<String> fileSink = FileSink.<String>forRowFormat(
                        new Path("outPut"),
                        new SimpleStringEncoder<>())
                .withOutputFileConfig(OutputFileConfig.builder()
                        .withPartPrefix("atguigu")
                        .withPartSuffix("log")
                        .build())
                .withBucketAssigner(new DateTimeBucketAssigner<>("yyyy-MM-dd HH"))
                .withRollingPolicy(DefaultRollingPolicy.builder()
                        .withRolloverInterval(Duration.ofSeconds(10))
                        .withMaxPartSize(new MemorySize(1024))
                        .build())
                .build();
        socketTextStream.sinkTo(fileSink);

        //启动任务
        env.execute();

    }
}
