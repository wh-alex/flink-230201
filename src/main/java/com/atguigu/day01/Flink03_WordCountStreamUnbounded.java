package com.atguigu.day01;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Flink03_WordCountStreamUnbounded {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取流执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamExecutionEnvironment env2 = StreamExecutionEnvironment.getExecutionEnvironment();
//        env.setParallelism()

        //TODO 2.从端口读取数据
        DataStreamSource<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        //TODO 3.压平
        SingleOutputStreamOperator<String> wordDS = socketTextStream.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public void flatMap(String value, Collector<String> out) throws Exception {
                String[] words = value.split(" ");
                for (String word : words) {
                    out.collect(word);
                }
            }
        });

        //TODO 4.将数据转换为元组
//        SingleOutputStreamOperator<Tuple2<String, Long>> wordToOneDS = wordDS.map(new MapFunction<String, Tuple2<String, Long>>() {
//            @Override
//            public Tuple2<String, Long> map(String value) throws Exception {
//                return new Tuple2<>(value, 1L);
//            }
//        });
        SingleOutputStreamOperator<Tuple2<String, Long>> wordToOneDS = wordDS
                .map(word -> new Tuple2<String, Long>(word, 1L))
                .returns(Types.TUPLE(Types.STRING, Types.LONG));

        //TODO 补充:
//        SingleOutputStreamOperator<Tuple2<String, Long>> wordToOneDS2 = socketTextStream.flatMap(new FlatMapFunction<String, Tuple2<String, Long>>() {
//            @Override
//            public void flatMap(String value, Collector<Tuple2<String, Long>> out) throws Exception {
//                String[] words = value.split(" ");
//                for (String word : words) {
//                    out.collect(new Tuple2<>(word, 1L));
//                }
//            }
//        });

        //TODO 5.分组
        KeyedStream<Tuple2<String, Long>, String> keyedStream = wordToOneDS.keyBy(new KeySelector<Tuple2<String, Long>, String>() {
            @Override
            public String getKey(Tuple2<String, Long> value) throws Exception {
                return value.f0;
            }
        });

        //TODO 6.聚合
        SingleOutputStreamOperator<Tuple2<String, Long>> sum = keyedStream.sum(1);

        //TODO 7.打印
        sum.print("env>>>");

        //关于env2的任务
        env2.socketTextStream("hadoop102", 8888)
                .flatMap(new FlatMapFunction<String, Tuple2<String, Long>>() {
                    @Override
                    public void flatMap(String value, Collector<Tuple2<String, Long>> out) throws Exception {
                        String[] words = value.split(" ");
                        for (String word : words) {
                            out.collect(new Tuple2<>(word, 1L));
                        }
                    }
                }).keyBy(new KeySelector<Tuple2<String, Long>, String>() {
                    @Override
                    public String getKey(Tuple2<String, Long> value) throws Exception {
                        return value.f0;
                    }
                }).sum(1)
                .print("env2>>>");

        //TODO 8.启动
        env.executeAsync();
        env2.execute();

    }
}
