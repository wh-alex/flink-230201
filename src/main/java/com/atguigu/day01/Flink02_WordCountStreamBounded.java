package com.atguigu.day01;

import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Flink02_WordCountStreamBounded {

    public static void main(String[] args) throws Exception {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setRuntimeMode(RuntimeExecutionMode.BATCH);
        //设置并行度
        env.setParallelism(1);

        //读取文件数据
        DataStreamSource<String> textFile = env.readTextFile("input/word.txt");

        //压平
        SingleOutputStreamOperator<String> wordDS = textFile.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public void flatMap(String value, Collector<String> out) throws Exception {
                String[] words = value.split(" ");
                for (String word : words) {
                    out.collect(word);
                }
            }
        }).setParallelism(3);

        //将单次转换为二元组
        SingleOutputStreamOperator<Tuple2<String, Long>> wordToOneDS = wordDS.map(new MapFunction<String, Tuple2<String, Long>>() {
            @Override
            public Tuple2<String, Long> map(String value) throws Exception {
                return new Tuple2<String, Long>(value, 1L);
            }
        });

        //分组
        KeyedStream<Tuple2<String, Long>, String> keyedByWordStream = wordToOneDS.keyBy(new KeySelector<Tuple2<String, Long>, String>() {
            @Override
            public String getKey(Tuple2<String, Long> value) throws Exception {
                return value.f0;
            }
        });

        //聚合
        SingleOutputStreamOperator<Tuple2<String, Long>> sum = keyedByWordStream.sum(1);

        //打印
        sum.print().setParallelism(1);

        //启动流程序,阻塞程序,让其一直运行,如果是有界数据流,最终内部会调用停止
        env.execute();

    }

}
