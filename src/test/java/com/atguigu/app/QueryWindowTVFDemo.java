package com.atguigu.app;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * @author Stephen
 * @create 2023-06-29-18:47
 */
public class QueryWindowTVFDemo {

    public static void main(String[] args) throws InterruptedException {

        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 2000);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(1);

        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        //tEnv.getConfig().

        //定义ddl时间属性
//        tEnv
//                .executeSql("create table sensor (" +
//                        " id string, " +
//                        " ts bigint, " +
//                        " vc int, " +
//                        " et as to_timestamp_ltz(ts, 3), " +
//                        " watermark for et as et - interval '1' second " +
//                        " ) with (" +
//                        " 'connector' = 'filesystem'," +
//                        " 'path' = 'input/sensor.txt'," +
//                        " 'format' = 'csv'" +
//                        " ) ");

        tEnv.executeSql("create table sensor(" +
                " id string, " +
                " ts bigint, " +
                " vc int, " +
                " et as to_timestamp_ltz(ts, 3), " +
                " watermark for et as et - interval '1' second " +
                ") with (" +
                "  'connector' = 'kafka', " +
                "  'topic' = 'test4', " +
                "  'properties.bootstrap.servers' = 'hadoop102:9092', " +
                "  'properties.group.id' = 'atguigu3', " +
                "  'scan.startup.mode' = 'latest-offset', " +
                "  'format' = 'csv' " +
                ")");


//        tEnv.from("sensor").execute().print();


        //滚动窗口
        tEnv
                .sqlQuery("select " +
                        "id, window_start, window_end, " +
                        "sum(vc) vc_sum " +
                        "from table (tumble (table sensor, descriptor(et), interval '5' second ) )" +
                        "group by id, window_start, window_end")
                .execute().print();


        //滑动窗口
        /*tEnv
                .sqlQuery("select " +
                        "id, window_start, window_end, " +
                        "sum(vc) vc_sum " +
                        "from table (hop (table sensor, descriptor(et), interval '2' second, interval '6' second ) )" +
                        "group by id, window_start, window_end")
                .execute()
                .print();*/

        //累积窗口
        //可以计算每隔1h计算当天pv
        //size must be an integral multiple of step.
//        tEnv
//                .sqlQuery("select " +
//                        "id, window_start, window_end, " +
//                        "sum(vc) vc_sum " +
//                        "from table (cumulate (table sensor, descriptor(et), interval '5' second, interval '20' second))" +
//                        "group by id, window_start, window_end")
//                .execute().print();

    }


}

