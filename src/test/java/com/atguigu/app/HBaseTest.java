package com.atguigu.app;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;

public class HBaseTest {

    public static void main(String[] args) throws IOException {

        Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", "hadoop102,hadoop103,hadoop104");
        Connection connection = ConnectionFactory.createConnection(configuration);

        //DDL
        //Admin admin = connection.getAdmin();

        //DML
        Table table = connection.getTable(TableName.valueOf("test_230201"));

        //创建Delete对象
        Delete delete = new Delete("1004".getBytes());

        //指定列族信息
//        delete.addFamily("f1".getBytes());

        //指定列信息
        //delete.addColumn();  //delete
        delete.addColumns("f1".getBytes(),
                "name".getBytes());

        table.delete(delete);

        //释放资源
        table.close();
        connection.close();

    }

}
